variable "aws_vpc_id" {
    type = string
    default = "vpc-094afeab7150376ce"
}

variable "aws_region" {
    type = string
    default = "us-west-2"
}

variable "release_tag" {
    type = string
    default = "DEV"
}


