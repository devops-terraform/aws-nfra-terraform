# Create DEV Infrastructure
# ----------------------------
# create a instance in AWS EC2
# AWS: 
#	(1) 
#		VPC 
#
#		Network ACLs
#		Security groups
#
#		Subnets
#		Routing tables
#
#		Internet Gateway
#		Egress-only Interna Gateway
#		NAT Gateway
#		Virtual private Gateway
#		Customer Gateway
#
#		Elastic IPs
#
#		S3
#		
#		Route 53
#		API Gateway
#
#		Elastic Container Service  - ECS
#		Elastic Kubernetes Service - EKS

#  Kubernets
#  Docker
#  Artifactory
#  Helm Chart

#
#
		



#	(2) Key pair
#	(3) Security group allow SSH

#	(4) create a user 'lnxcfg' for Ansible Server

#	(5) Create once EC2 Instance for Jenkins
#	(5) Create once EC2 Instance for Rancher

#	(5) Create once EC2 Instance for Kubernetes Master
#	(6) Create once EC2 Instance for Kubernetes Node1
#	(7) Create once EC2 Instance for Kubernetes Node2

#	(8) 
#
#

# create a user for connect : lnxcfg
# install packages: java 1.8, nginx
# install Jenkins
# install 

terraform {

backend "s3" {
    bucket         = "my-infra-terraform-tfstates"
    key            = "global/s3/terraform.tfstate"
    region         = "us-west-2"
  }

  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.aws_region

}


resource "aws_security_group" "sgdev" {

  name        = "sgdev"
  description = "Allow inbound SSH traffic from my IP"
  vpc_id      = var.aws_vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_key_pair" "devkeypair" {
  key_name   = "devkeypair"
  public_key = file("~/.ssh/id_rsa.pub")
}


resource "aws_instance" "jenkins" {
  key_name      = aws_key_pair.devkeypair.key_name
  ami           = "ami-04590e7389a6e577c"
  instance_type = "t2.micro"
  associate_public_ip_address = true
  subnet_id = "subnet-0e40f520c86c2951f"
  vpc_security_group_ids = [aws_security_group.sgdev.id]

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file("~/.ssh/id_rsa")
    host        = self.public_ip
  }

 provisioner "file" {
    source      = "setup-lnxcfg-user"
    destination = "/tmp/setup-lnxcfg-user"
  }

tags = {
    Name = var.release_tag
  } 


  provisioner "remote-exec" {
    inline = [
      "sudo amazon-linux-extras enable nginx1.12",
      "sudo yum -y install nginx",
      "sudo systemctl start nginx",
      "sudo yum -y install java-1.8.0",
      "sudo yum update –y",
      "sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat/jenkins.repo",
      "sudo rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key",
      "sudo yum install jenkins -y",
      "sudo systemctl start jenkins.service",
      "sudo chkconfig --add jenkins",
      "chmod +x /tmp/setup-lnxcfg-user",
      "sudo /tmp/setup-lnxcfg-user"
    ]
  }

  
}

